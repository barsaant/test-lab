import requests
import _thread

url = "http://logcalhost:3000"
request_count = 1000
thread_count = 1

def http_flood():
  count = 0
  while count < request_count:
    response = requests.get(url)
    print(response)
    count += 1

try:
  thread = 0 
  while thread < thread_count:
    _thread.start_new_thread(http_flood())
    thread += 1
except:
  print("Error")